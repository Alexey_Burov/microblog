from django.http import HttpResponseRedirect
from django.core.urlresolvers import reverse

from django.shortcuts import render
from .models import Topic, Entry
from .forms import TopicForm, EntryForm

def index(request):
    """home page app task"""
    # return render(request, 'task/index.html')
    topics = Topic.objects.order_by('date_added')
    context = {'topics': topics}
    return render(request, 'task/topics.html', context)

def topics(request):
    topics = Topic.objects.order_by('date_added')
    context = {'topics': topics}
    return render(request, 'task/topics.html', context)


def topic(request, topic_id):
    topic = Topic.objects.get(id=topic_id)
    entries = topic.entry_set.order_by('-date_added')
    context = {'topic': topic, 'entries': entries}
    return render(request, 'task/topic.html', context)


def new_topic(request):
    """new theme"""
    if request.method != 'POST':
    # no send data empty form
        form = TopicForm()
    else:
    # send POST; data hendler.
        form = TopicForm(request.POST)
    if form.is_valid():
        form.save()
        return HttpResponseRedirect(reverse('task:topics'))

    context = {'form': form}
    return render(request, 'task/new_topic.html', context)


def new_entry(request, topic_id):
    """add new write."""
    topic = Topic.objects.get(id=topic_id)
    if request.method != 'POST':
    # no send data empty form
        form = EntryForm()
    else:
    # send POST; data hendler.
        form = EntryForm(data=request.POST)
        if form.is_valid():
            new_entry = form.save(commit=False)
            new_entry.topic = topic
            new_entry.save()
            return HttpResponseRedirect(reverse('task:topic',args=[topic_id]))
    context = {'topic': topic, 'form': form}
    return render(request, 'task/new_entry.html', context)


def edit_entry(request, entry_id):
    """edit."""

    entry = Entry.objects.get(id=entry_id)
    topic = entry.topic

    if request.method != 'POST':
        # form current data.
        form = EntryForm(instance=entry)
    else:
        # send POST; data hendler.
        form = EntryForm(instance=entry, data=request.POST)
        if form.is_valid():
            form.save()
            return HttpResponseRedirect(reverse('task:topic', args=[topic.id]))
    context = {'entry': entry, 'topic': topic, 'form': form}
    return render(request, 'task/edit_entry.html', context)

# Create your views here.
