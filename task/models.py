from django.db import models


class Topic(models.Model):
    '''user thhem'''
    text = models.CharField(max_length=200)
    date_added = models.DateTimeField(auto_now_add=True)
    ''':returns model to string'''
    def __str__(self):
        return self.text


class Entry(models.Model):
    """user information them"""
    topic = models.ForeignKey(Topic)
    text = models.TextField()
    date_added = models.DateTimeField(auto_now_add=True)

    class Meta:
        verbose_name_plural = 'entries'

        def __str__(self):
            """:returns model to string"""
            return self.text[:50] + "..."

# Create your models here.
