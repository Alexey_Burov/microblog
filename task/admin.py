from django.contrib import admin
from task.models import Topic, Entry

admin.site.register(Topic)
admin.site.register(Entry)
# Register your models here.
