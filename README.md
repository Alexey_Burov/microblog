##***Микроблог***

пgриложение написано на языке Python функционирует под управлением фреймворка Django.

### Установка

####`Установка PostgreSQL`
    
    #! заголовочные  файлы для libpq5 (библиотеки PostgreSQL):
    sudo apt-get install libpq-dev
    
    #! СУБД PostgreSQL:
    sudo apt-get install postgresql
    
    #! дополнения PostgreSQL:
    sudo apt-get install postgresql-contrib
    
    #! запуск оболочки PostgreSQL:
    sudo -u postgres psql

`далее в консоли psql:`


    -- база данных
    CREATE DATABASE myproject; 
    
    -- пользователь
    CREATE USER myprojectuser WITH PASSWORD 'password';
    
    -- установка кодировки
    ALTER ROLE myprojectuser SET client_encoding TO 'utf8';
    
    -- установка уровеня изоляции в базе данных
    ALTER ROLE myprojectuser SET default_transaction_isolation TO 'read committed';
    
    -- установка типа временных зон
    ALTER ROLE myprojectuser SET timezone TO 'UTC';
    
    -- Важно! права доступа к базе данных для нового пользователя
    GRANT ALL PRIVILEGES ON DATABASE myproject TO myprojectuser;

####`Установка виртуального окружения `

    pip3 install virtualenv
    virtualenv ~/myprojectenv
    source myprojectenv/bin/activate

####`Клонирование проекта`

    git@gitlab.com:Alexey_Burov/microblog.git

    либо

    https://gitlab.com/Alexey_Burov/microblog.git

####`Установка драйвера PostgreSQL`

    sudo pip install django psycopg2

#### `Формирование структуры БД`

    cd /myproject
    python manage.py makemigrations
    python manage.py migrate

### Запуск

    python manage.py runserver

    #! либо внутри виртуального окружения:    
    python2 manage.py runserver

### Использование

    
    Записи объединены в темы. Темы можно добавлять.

[](screenshots/1.png)

    В тему можно добавить несколько записей. Запись можно перекрасить.
    
[](screenshots/2.png)